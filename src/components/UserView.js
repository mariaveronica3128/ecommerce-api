import React, { useState,useEffect } from "react";
import { Container } from "react-bootstrap";
import ProductCard from "./ProductCard";

export default function UserView ({productData}){

    const [products , setProduct] = useState([])
    useEffect(()=>{
        const productArr = productData.map(product =>{
            if(product.isActive === true){
                return(
                    <ProductCard productProp={product} key={product._id}/>
                )
            } else{
                return null;
            } 
        })
        setProduct(productArr);
    },[productData])
    return(
    <>
    {products}

    <Container>

    <h1> Back to HomePage</h1>
    </Container>
    </>

    )
}
