import React, { useEffect, useState } from "react";

import { Container, Table } from "react-bootstrap";
// import ArchiveProduct from "./ArchiveProduct";
import AddProduct from "./AddProduct";

export default function AdminView(props) {
    const { productData, fetchData } = props;
    const [products, setProduct] = useState([]);

  useEffect(() => {
    const productsArr = productData.map((product) => {
      return (
        <tr key={product._id}>
            <td>{product._id}</td>
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.category}</td>
            <td>{product.countInStock}</td>
            <td>{product.price}</td>
            <td className={product.isActive ? "text-success" : "text-danger"}>
                {product.isActive ? "Available" : "Unavailable"}
            </td>
            {/* <td><EditCourse product={product._id} fetchData={fetchData}/></td>	 */}
            {/* <td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td> */}
         </tr>
     );
    });
    setProduct(productsArr);
    }, [productData]);

    return (
    <Container>
      <div className="text-center-my-4">
        <h1>Admin Dashboard</h1>
        <AddProduct fetchData={fetchData} />
      </div>

      <Table>
        <thead striped bordered hover responsive>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availabilty</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{products}</tbody>
      </Table>
    </Container>
  );
}
