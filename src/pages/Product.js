import React, { useState,useContext,useEffect } from "react";
import UserContext from "../UserContext";
import UserView from "../components/UserView";
import AdminView from "../components/AdminView";

export default function Product(){

const {user} = useContext(UserContext);

const [allProducts, setAllProducts] = useState([]);

console.log(user)
    const fetchData = () => {
        fetch("http://localhost:4000/products/aItems")
          .then((result) => result.json())
          .then((data) => {
            console.log(data)
              setAllProducts(data);
        });
          
      };
      useEffect(() => {
        fetchData();
        
      }, []);

      return (
        <>
          {user.isAdmin === true ?
          (<AdminView productData={allProducts} fetchData={fetchData} />) : 
          (<UserView productData={allProducts} />)}
        </>
      );
}
